
main()
{
	spawnpointname = "mp_searchanddestroy_spawn_allied";
	spawnpoints = getentarray(spawnpointname, "classname");
	
	if(!spawnpoints.size)
	{
		maps\mp\gametypes\_callbacksetup::AbortLevel();
		return;
	}

	for(i = 0; i < spawnpoints.size; i++)
		spawnpoints[i] placeSpawnpoint();

	spawnpointname = "mp_searchanddestroy_spawn_axis";
	spawnpoints = getentarray(spawnpointname, "classname");

	if(!spawnpoints.size)
	{
		maps\mp\gametypes\_callbacksetup::AbortLevel();
		return;
	}

	for(i = 0; i < spawnpoints.size; i++)
		spawnpoints[i] PlaceSpawnpoint();

	level.callbackStartGameType = ::Callback_StartGameType;
	level.callbackPlayerConnect = ::Callback_PlayerConnect;
	level.callbackPlayerDisconnect = ::Callback_PlayerDisconnect;
	level.callbackPlayerDamage = ::Callback_PlayerDamage;
	level.callbackPlayerKilled = ::Callback_PlayerKilled;

	maps\mp\gametypes\_callbacksetup::SetupCallbacks();

	level._effect["bombexplosion"] = loadfx("fx/explosions/mp_bomb.efx");

	allowed[0] = "sd";
	allowed[1] = "bombzone";
	allowed[2] = "blocker";
	maps\mp\gametypes\_gameobjects::main(allowed);
	
	if(getCvar("scr_sd_timelimit") == "")		// Time limit per map
		setCvar("scr_sd_timelimit", "0");
	else if(getCvarFloat("scr_sd_timelimit") > 1440)
		setCvar("scr_sd_timelimit", "1440");
	level.timelimit = getCvarFloat("scr_sd_timelimit");
	setCvar("ui_sd_timelimit", level.timelimit);
	makeCvarServerInfo("ui_sd_timelimit", "0");

	if(!isDefined(game["timepassed"]))
		game["timepassed"] = 0;

	if(getCvar("scr_sd_scorelimit") == "")		// Score limit per map
		setCvar("scr_sd_scorelimit", "10");
	level.scorelimit = getCvarInt("scr_sd_scorelimit");
	setCvar("ui_sd_scorelimit", level.scorelimit);
	makeCvarServerInfo("ui_sd_scorelimit", "10");

	if(getCvar("scr_sd_roundlimit") == "")		// Round limit per map
		setCvar("scr_sd_roundlimit", "0");
	level.roundlimit = getCvarInt("scr_sd_roundlimit");
	setCvar("ui_sd_roundlimit", level.roundlimit);
	makeCvarServerInfo("ui_sd_roundlimit", "0");

	if(getCvar("scr_sd_roundlength") == "")		// Time length of each round
		setCvar("scr_sd_roundlength", "4");
	else if(getCvarFloat("scr_sd_roundlength") > 10)
		setCvar("scr_sd_roundlength", "10");
	level.roundlength = getCvarFloat("scr_sd_roundlength");

	if(getCvar("scr_sd_graceperiod") == "")		// Time at round start where spawning and weapon choosing is still allowed
		setCvar("scr_sd_graceperiod", "15");
	else if(getCvarFloat("scr_sd_graceperiod") > 60)
		setCvar("scr_sd_graceperiod", "60");
	level.graceperiod = getCvarFloat("scr_sd_graceperiod");

	killcam = getCvar("scr_killcam");
	if(killcam == "")				// Kill cam
		killcam = "1";
	setCvar("scr_killcam", killcam, true);
	level.killcam = getCvarInt("scr_killcam");
	
	if(getCvar("scr_teambalance") == "")		// Auto Team Balancing
		setCvar("scr_teambalance", "0");
	level.teambalance = getCvarInt("scr_teambalance");
	level.lockteams = false;

	if(getCvar("scr_freelook") == "")		// Free look spectator
		setCvar("scr_freelook", "1");
	level.allowfreelook = getCvarInt("scr_freelook");
	
	if(getCvar("scr_spectateenemy") == "")		// Spectate Enemy Team
		setCvar("scr_spectateenemy", "1");
	level.allowenemyspectate = getCvarInt("scr_spectateenemy");
	
	if(getCvar("scr_drawfriend") == "")		// Draws a team icon over teammates
		setCvar("scr_drawfriend", "0");
	level.drawfriend = getCvarInt("scr_drawfriend");

	if(!isDefined(game["state"]))
		game["state"] = "playing";
	if(!isDefined(game["roundsplayed"]))
		game["roundsplayed"] = 0;
	if(!isDefined(game["matchstarted"]))
		game["matchstarted"] = false;
		
	if(!isDefined(game["alliedscore"]))
		game["alliedscore"] = 0;
	setTeamScore("allies", game["alliedscore"]);

	if(!isDefined(game["axisscore"]))
		game["axisscore"] = 0;
	setTeamScore("axis", game["axisscore"]);

	level.bombplanted = false;
	level.bombexploded = false;
	level.roundstarted = false;
	level.roundended = false;
	level.mapended = false;
	
	if (!isdefined (game["BalanceTeamsNextRound"]))
		game["BalanceTeamsNextRound"] = false;
	
	level.exist["allies"] = 0;
	level.exist["axis"] = 0;
	level.exist["teams"] = false;
	level.didexist["allies"] = false;
	level.didexist["axis"] = false;

	if(level.killcam >= 1)
		setarchive(true);
}

Callback_StartGameType()
{
	// if this is a fresh map start, set nationalities based on cvars, otherwise leave game variable nationalities as set in the level script
	if(!isDefined(game["gamestarted"]))
	{
		// defaults if not defined in level script
		if(!isDefined(game["allies"]))
			game["allies"] = "american";
		if(!isDefined(game["axis"]))
			game["axis"] = "german";

		if(!isDefined(game["layoutimage"]))
			game["layoutimage"] = "default";
		layoutname = "levelshots/layouts/hud@layout_" + game["layoutimage"];
		precacheShader(layoutname);
		setCvar("scr_layoutimage", layoutname);
		makeCvarServerInfo("scr_layoutimage", "");

		// server cvar overrides
		if(getCvar("scr_allies") != "")
			game["allies"] = getCvar("scr_allies");	
		if(getCvar("scr_axis") != "")
			game["axis"] = getCvar("scr_axis");

		game["menu_team"] = "team_" + game["allies"] + game["axis"];
		game["menu_viewmap"] = "viewmap";
		game["menu_callvote"] = "callvote";

		precacheString(&"MPSCRIPT_PRESS_ACTIVATE_TO_SKIP");
		precacheString(&"MPSCRIPT_KILLCAM");
		precacheString(&"SD_MATCHSTARTING");
		precacheString(&"SD_MATCHRESUMING");
		precacheString(&"SD_EXPLOSIVESPLANTED");
		precacheString(&"SD_EXPLOSIVESDEFUSED");
		precacheString(&"SD_ROUNDDRAW");
		precacheString(&"SD_TIMEHASEXPIRED");
		precacheString(&"SD_ALLIEDMISSIONACCOMPLISHED");
		precacheString(&"SD_AXISMISSIONACCOMPLISHED");
		precacheString(&"SD_ALLIESHAVEBEENELIMINATED");
		precacheString(&"SD_AXISHAVEBEENELIMINATED");

		precacheMenu(game["menu_team"]);
		precacheMenu(game["menu_viewmap"]);
		precacheMenu(game["menu_callvote"]);
		
		precacheShader("black");
		precacheShader("white");
		precacheShader("hudScoreboard_mp");
		precacheShader("gfx/hud/hud@mpflag_spectator.tga");
		precacheStatusIcon("gfx/hud/hud@status_dead.tga");
		precacheStatusIcon("gfx/hud/hud@status_connecting.tga");

		precacheShader("ui_mp/assets/hud@plantbomb.tga");
		precacheShader("ui_mp/assets/hud@defusebomb.tga");
		precacheShader("gfx/hud/hud@objectiveA.tga");
		precacheShader("gfx/hud/hud@objectiveA_up.tga");
		precacheShader("gfx/hud/hud@objectiveA_down.tga");
		precacheShader("gfx/hud/hud@objectiveB.tga");
		precacheShader("gfx/hud/hud@objectiveB_up.tga");
		precacheShader("gfx/hud/hud@objectiveB_down.tga");
		precacheShader("gfx/hud/hud@bombplanted.tga");
		precacheShader("gfx/hud/hud@bombplanted_up.tga");
		precacheShader("gfx/hud/hud@bombplanted_down.tga");
		precacheShader("gfx/hud/hud@bombplanted_down.tga");
		precacheModel("xmodel/mp_bomb1_defuse");
		precacheModel("xmodel/mp_bomb1");
		
		// cashmodcode	
		cm_setupGameVariables();
		
		maps\mp\gametypes\_teams::precache();
		maps\mp\gametypes\_teams::scoreboard();
		
		thread addBotClients();
	}
	
	maps\mp\gametypes\_teams::modeltype();
	maps\mp\gametypes\_teams::initGlobalCvars();
	maps\mp\gametypes\_teams::initWeaponCvars();
	maps\mp\gametypes\_teams::restrictPlacedWeapons();
	thread maps\mp\gametypes\_teams::updateGlobalCvars();
	thread maps\mp\gametypes\_teams::updateWeaponCvars();

	game["gamestarted"] = true;
	
	setClientNameMode("manual_change");

	thread bombzones();
	thread startGame();
	thread updateGametypeCvars();
	//thread addBotClients();
}







Callback_PlayerConnect()
{
	self.statusicon = "gfx/hud/hud@status_connecting.tga";
	self waittill("begin");
	self.statusicon = "";
	self.pers["teamTime"] = 1000000;

	if(!isDefined(self.pers["team"]))
		iprintln(&"MPSCRIPT_CONNECTED", self);

	lpselfnum = self getEntityNumber();
	lpselfguid = self getGuid();
	logPrint("J;" + lpselfguid + ";" + lpselfnum + ";" + self.name + "\n");

	if(game["state"] == "intermission")
	{
		spawnIntermission();
		return;
	}
	
	level endon("intermission");
	
	if(isDefined(self.pers["team"]) && self.pers["team"] != "spectator")
	{
		spawnPlayer();
	}
	else
	{
		self setClientCvar("g_scriptMainMenu", game["menu_team"]);
		self openMenu(game["menu_team"]);

		self.pers["team"] = "spectator";
		self.sessionteam = "spectator";

		spawnSpectator();
	}
	
	cm_menuResponse();
}

cm_menuResponse()
{
	for(;;)
	{
		self waittill("menuresponse", menu, response);

		if(response == "open" || response == "close")
			continue;
	
	
	
		// TEAM MENU STUFF
		if(menu == game["menu_team"])
		{
			switch(response)
			{
				case "allies":
				case "axis":
				case "autoassign":
					if (level.lockteams)
						break;
					if(response == "autoassign")
					{
						numonteam["allies"] = 0;
						numonteam["axis"] = 0;

						players = getentarray("player", "classname");
						for(i = 0; i < players.size; i++)
						{
							player = players[i];
						
							if(!isDefined(player.pers["team"]) || player.pers["team"] == "spectator" || player == self)
								continue;
				
							numonteam[player.pers["team"]]++;
						}
						
						// if teams are equal return the team with the lowest score
						if(numonteam["allies"] == numonteam["axis"])
						{
							if(getTeamScore("allies") == getTeamScore("axis"))
							{
								teams[0] = "allies";
								teams[1] = "axis";
								response = teams[randomInt(2)];
							}
							else if(getTeamScore("allies") < getTeamScore("axis"))
								response = "allies";
							else
								response = "axis";
						}
						else if(numonteam["allies"] < numonteam["axis"])
							response = "allies";
						else
							response = "axis";
						skipbalancecheck = true;
					}
					
					if(response == self.pers["team"] && self.sessionstate == "playing")
						break;
					
					//Check if the teams will become unbalanced when the player goes to this team...
					//------------------------------------------------------------------------------
					if ( (level.teambalance > 0) && (!isdefined (skipbalancecheck)) )
					{
						//Get a count of all players on Axis and Allies
						players = maps\mp\gametypes\_teams::CountPlayers();
						
						if (self.sessionteam != "spectator")
						{
							if (((players[response] + 1) - (players[self.pers["team"]] - 1)) > level.teambalance)
							{
								if (response == "allies")
								{
									if (game["allies"] == "american")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_ALLIED",&"PATCH_1_3_AMERICAN");
									else if (game["allies"] == "british")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_ALLIED",&"PATCH_1_3_BRITISH");
									else if (game["allies"] == "russian")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_ALLIED",&"PATCH_1_3_RUSSIAN");
								}
								else
									self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_ALLIED",&"PATCH_1_3_GERMAN");
								break;
							}
						}
						else
						{
							if (response == "allies")
								otherteam = "axis";
							else
								otherteam = "allies";
							if (((players[response] + 1) - players[otherteam]) > level.teambalance)
							{
								if (response == "allies")
								{
									if (game["allies"] == "american")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_ALLIED2",&"PATCH_1_3_AMERICAN");
									else if (game["allies"] == "british")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_ALLIED2",&"PATCH_1_3_BRITISH");
									else if (game["allies"] == "russian")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_ALLIED2",&"PATCH_1_3_RUSSIAN");
								}
								else
								{
									if (game["allies"] == "american")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_AXIS",&"PATCH_1_3_AMERICAN");
									else if (game["allies"] == "british")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_AXIS",&"PATCH_1_3_BRITISH");
									else if (game["allies"] == "russian")
										self iprintlnbold(&"PATCH_1_3_CANTJOINTEAM_AXIS",&"PATCH_1_3_RUSSIAN");
								}
								break;
							}
						}
					}
					skipbalancecheck = undefined;
					//------------------------------------------------------------------------------
					
					if(response != self.pers["team"] && self.sessionstate == "playing")
					{
						// self.pers["isplaying"] = 0;
						self suicide();
					}
								
					self.pers["team"] = response;
					self.pers["teamTime"] = (gettime() / 1000);
					self.pers["savedmodel"] = undefined;
					
					// cashmodcode
					self cm_clearPlayerWeapons();
					self cm_clearPlayerMoney();
					self.pers["isplaying"] = 0;

					// update spectator permissions immediately on change of team
					maps\mp\gametypes\_teams::SetSpectatePermissions();

					if(self.pers["team"] == "allies")
					{
						self.spawned = undefined;
						spawnPlayer();
						self thread printJoinedTeam(self.pers["team"]);
					}
					else
					{
						self.spawned = undefined;
						spawnPlayer();
						self thread printJoinedTeam(self.pers["team"]);
					}
					
					break;
					
				case "spectator" :
					if (level.lockteams)
					break;
					
					// cashmodcode
					self cm_clearPlayerWeapons();
					self cm_clearPlayerMoney();
					self.pers["isplaying"] = 0;
					
					if(self.pers["team"] != "spectator")
					{
						if(isAlive(self))
							self suicide();

						self.pers["team"] = "spectator";
						self.pers["teamTime"] = 1000000;
						self.pers["savedmodel"] = undefined;

						self.sessionteam = "spectator";
						self setClientCvar("g_scriptMainMenu", game["menu_team"]);
						spawnSpectator();
					}
					break;
					
				case "viewmap":
					self openMenu(game["menu_viewmap"]);
					break;
			
				case "callvote":
					self openMenu(game["menu_callvote"]);
					break;
			}
		}		
		
		
		// VIEW MAP
		else if (menu == game["menu_viewmap"])
		{
			if (response == "team")
				self openMenu(game["menu_team"]);
				
			else if (response == "callvote")
				self openMenu(game["menu_callvote"]);
		}
		
		
		// VOTE MENU
		else if (menu == game["menu_callvote"])
		{
			if (response == "team")
				self openMenu(game["menu_team"]);
				
			else if (response == "viewmap")
				self openMenu(game["menu_viewmap"]);
		}
		
		
		// DROP WEAPON 
		if (response == "dropweapon")
			cm_dropCurrentWeapon();
			
		
	
		// QUICK WEAPON MENU STUFF
		// Things can only be bought within the grace period
		else if (cm_withinGracePeriod())
		{
			if (response == "primarymagazine")
				cm_buyPrimaryMagazine();
				
			else if (response == "secondarymagazine")
				cm_buySecondaryMagazine();
				
			else if (response == "pistolmagazine")
				cm_buyPistolMagazine();
				
			else if (response == "grenade")
				cm_buyGrenade();
				
			
			else if (menu == game["cm_rifles"])
				cm_buyWeapon(response);
				
			else if (menu == game["cm_machineguns2"])
				cm_buyWeapon(response);
		}
		else
			iprintln("You can only buy at the start of a round");
	}
}

spawnPlayer()
{
	self notify("spawned");

	resettimeout();

	self.sessionteam = self.pers["team"];
	self.spectatorclient = -1;
	self.archivetime = 0;
	self.friendlydamage = undefined;

	if(isDefined(self.spawned))
		return;

	self.sessionstate = "playing";
		
	if(self.pers["team"] == "allies")
		spawnpointname = "mp_searchanddestroy_spawn_allied";
	else
		spawnpointname = "mp_searchanddestroy_spawn_axis";

	spawnpoints = getentarray(spawnpointname, "classname");
	spawnpoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_Random(spawnpoints);

	if(isDefined(spawnpoint))
		self spawn(spawnpoint.origin, spawnpoint.angles);
	else
		maps\mp\_utility::error("NO " + spawnpointname + " SPAWNPOINTS IN MAP");
	
	self.spawned = true;
	self.statusicon = "";
	self.maxhealth = 100;
	self.health = self.maxhealth;
	
	updateTeamStatus();
	if (!game["matchstarted"])
		checkMatchStart();
	
	if(!isDefined(self.pers["score"]))
		self.pers["score"] = 0;
	self.score = self.pers["score"];
	
	if(!isDefined(self.pers["deaths"]))
		self.pers["deaths"] = 0;
	self.deaths = self.pers["deaths"];
	
	if(!isDefined(self.pers["savedmodel"]))
		maps\mp\gametypes\_teams::model();
	else
		maps\mp\_utility::loadModel(self.pers["savedmodel"]);		
	
	
	self.usedweapons = false;
	thread maps\mp\gametypes\_teams::watchWeaponUsage();

	if(self.pers["team"] == game["attackers"])
		self setClientCvar("cg_objectiveText", &"SD_OBJ_ATTACKERS");
	else if(self.pers["team"] == game["defenders"])
		self setClientCvar("cg_objectiveText", &"SD_OBJ_DEFENDERS");
		
	if(level.drawfriend)
	{
		if(self.pers["team"] == "allies")
		{
			self.headicon = game["headicon_allies"];
			self.headiconteam = "allies";
		}
		else
		{
			self.headicon = game["headicon_axis"];
			self.headiconteam = "axis";
		}
	}
	
	// cashmodcode	
	if ( !self.pers["isplaying"] == 1 )
	{
		self cm_clearPlayerWeapons();
		self cm_setupPlayerMoney();		
	}
	else
	{
		self cm_loadPlayerWeapons();
	}
	
	self cm_givePistolNoAmmo();
	self cm_switchToMainWeapon();
	self cm_updateHud();	
	
	self.pers["isplaying"] = 1;
}

Callback_PlayerKilled(eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc)
{
	self endon("spawned");

	if(self.sessionteam == "spectator")
		return;

	// If the player was killed by a head shot, let players know it was a head shot kill
	if(sHitLoc == "head" && sMeansOfDeath != "MOD_MELEE")
		sMeansOfDeath = "MOD_HEAD_SHOT";

	// send out an obituary message to all clients about the kill
	obituary(self, attacker, sWeapon, sMeansOfDeath);

	self.sessionstate = "dead";
	self.statusicon = "gfx/hud/hud@status_dead.tga";
	self.headicon = "";
	if (!isdefined (self.autobalance))
	{
		self.pers["deaths"]++;
		self.deaths = self.pers["deaths"];
	}

	lpselfnum = self getEntityNumber();
	lpselfguid = self getGuid();
	lpselfname = self.name;
	lpselfteam = self.pers["team"];
	lpattackerteam = "";

	attackerNum = -1;

	if(isPlayer(attacker))
	{
		if(attacker == self) // killed himself
		{
			doKillcam = false;
			if (!isdefined (self.autobalance))
			{
				attacker.pers["score"]--;
				attacker.score = attacker.pers["score"];
			}
			
			if(isDefined(attacker.friendlydamage))
				clientAnnouncement(attacker, &"MPSCRIPT_FRIENDLY_FIRE_WILL_NOT"); 
		}
		else
		{
			attackerNum = attacker getEntityNumber();
			doKillcam = true;

			if(self.pers["team"] == attacker.pers["team"]) // killed by a friendly
			{
				attacker.pers["score"]--;
				attacker.score = attacker.pers["score"];
				
				// cashmodcode
				amount = cm_getMoneyReward("teamkill");
				attacker cm_updateMoney("reward", "teamkill", amount);
			}
			else
			{
				attacker.pers["score"]++;
				attacker.score = attacker.pers["score"];
				
				// cashmodcode
				amount = cm_getMoneyReward("frag");
				attacker cm_updateMoney("reward", "frag", amount);
			}
		}
		
		lpattacknum = attacker getEntityNumber();
		lpattackguid = attacker getGuid();
		lpattackname = attacker.name;
		lpattackerteam = attacker.pers["team"];
	}
	else // If you weren't killed by a player, you were in the wrong place at the wrong time
	{
		doKillcam = false;

		self.pers["score"]--;
		self.score = self.pers["score"];

		lpattacknum = -1;
		lpattackguid = "";
		lpattackname = "";
		lpattackerteam = "world";
	}

	logPrint("K;" + lpselfguid + ";" + lpselfnum + ";" + lpselfteam + ";" + lpselfname + ";" + lpattackguid + ";" + lpattacknum + ";" + lpattackerteam + ";" + lpattackname + ";" + sWeapon + ";" + iDamage + ";" + sMeansOfDeath + ";" + sHitLoc + "\n");

	// Make the player drop his weapon
	if (!isdefined (self.autobalance))
		self dropItem(self getcurrentweapon());
	
	if (!isdefined (self.autobalance))
		body = self cloneplayer();
	self.autobalance = undefined;

	updateTeamStatus();

	// TODO: Add additional checks that allow killcam when the last player killed wouldn't end the round (bomb is planted)
	if((getCvarInt("scr_killcam") <= 0) || !level.exist[self.pers["team"]]) // If the last player on a team was just killed, don't do killcam
		doKillcam = false;

	delay = 2;	// Delay the player becoming a spectator till after he's done dying
	wait delay;	// ?? Also required for Callback_PlayerKilled to complete before killcam can execute

	if(doKillcam && !level.roundended)
		self thread killcam(attackerNum, delay);
	else
	{
		currentorigin = self.origin;
		currentangles = self.angles;

		self thread spawnSpectator(currentorigin + (0, 0, 60), currentangles);
	}
	
	// cashmodcode
	self cm_clearPlayerWeapons();
}

spawnSpectator(origin, angles)
{
	self notify("spawned");

	resettimeout();

	self.sessionstate = "spectator";
	self.spectatorclient = -1;
	self.archivetime = 0;
	self.friendlydamage = undefined;

	if(self.pers["team"] == "spectator")
		self.statusicon = "";
		
	maps\mp\gametypes\_teams::SetSpectatePermissions();

	if(isDefined(origin) && isDefined(angles))
		self spawn(origin, angles);
	else
	{
 		spawnpointname = "mp_searchanddestroy_intermission";
		spawnpoints = getentarray(spawnpointname, "classname");
		spawnpoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_Random(spawnpoints);

		if(isDefined(spawnpoint))
			self spawn(spawnpoint.origin, spawnpoint.angles);
		else
			maps\mp\_utility::error("NO " + spawnpointname + " SPAWNPOINTS IN MAP");
	}

	updateTeamStatus();

	self.usedweapons = false;

	if(game["attackers"] == "allies")
		self setClientCvar("cg_objectiveText", &"SD_OBJ_SPECTATOR_ALLIESATTACKING");
	else if(game["attackers"] == "axis")
		self setClientCvar("cg_objectiveText", &"SD_OBJ_SPECTATOR_AXISATTACKING");
		
	self setClientCvar("g_scriptMainmenu", game["menu_team"]);	
}






startGame()
{
	level.starttime = getTime();
	
	// if (!isDefined(game["matchstarted"]) || !game["matchstarted"])
	// {
		// checkMatchStart();
		// // return;
	// }
	
	thread startRound();
	
	if ( (level.teambalance > 0) && (!game["BalanceTeamsNextRound"]) )
		level thread maps\mp\gametypes\_teams::TeamBalance_Check_Roundbased();
}

checkMatchStart()
{
	if (game["matchstarted"])
		return;
		
	// If there are players on both teams, start the match
	if(level.exist["allies"] && level.exist["axis"])
	{
		announcement(&"SD_MATCHSTARTING");
		
		wait(3);

		level notify("kill_endround");
		level.roundended = false;
		
		resetScores();
		game["roundsplayed"] = 0;
		
		cm_setupGameVariables();
		players = getentarray("player", "classname");
		for (i = 0; i < players.size; i++)
		{
			player = players[i];
			player cm_setupPlayer();
		}
		
		map_restart(true);
		
		game["matchstarted"] = true;
	}

}

startRound()
{
	//cashmodcode	
	self thread cm_givePistolNoAmmo();
	self thread cm_updateHud();
	

	level endon("bomb_planted");

	thread maps\mp\gametypes\_teams::sayMoveIn();

	

	level.clock = newHudElem();
	level.clock.x = 320;
	level.clock.y = 460;
	level.clock.alignX = "center";
	level.clock.alignY = "middle";
	level.clock.font = "bigfixed";
	level.clock setTimer(level.roundlength * 60);

	if(game["matchstarted"])
	{
		level.clock.color = (0, 1, 0);

		if((level.roundlength * 60) > level.graceperiod)
		{
			wait level.graceperiod;

			level notify("round_started");
			level.roundstarted = true;
			level.clock.color = (1, 1, 1);

			// Players on a team but without a weapon show as dead since they can not get in this round
			players = getentarray("player", "classname");
			for(i = 0; i < players.size; i++)
			{
				player = players[i];

				if(player.sessionteam != "spectator" && !isDefined(player.pers["weapon"]))
					player.statusicon = "gfx/hud/hud@status_dead.tga";
			}
		
			wait((level.roundlength * 60) - level.graceperiod);
		}
		else
			wait(level.roundlength * 60);
	}
	else	
	{
		level.clock.color = (1, 1, 1);
		wait(level.roundlength * 60);
	}
	
	if(level.roundended)
		return;

	if(!level.exist[game["attackers"]] || !level.exist[game["defenders"]])
	{
		announcement(&"SD_TIMEHASEXPIRED");
		level thread endRound("draw");
		return;
	}

	announcement(&"SD_TIMEHASEXPIRED");
	level thread endRound(game["defenders"]);
}

endRound(roundwinner)
{
	level endon("kill_endround");

	if(level.roundended)
		return;
	level.roundended = true;

	// End bombzone threads and remove related hud elements and objectives
	level notify("round_ended");	

	players = getentarray("player", "classname");
	for(i = 0; i < players.size; i++)
	{
		player = players[i];		
		
		// cashmodcode
		if (player.pers["isplaying"])
			player thread cm_endRoundMoney(roundwinner);
		
		if(isDefined(player.planticon))
			player.planticon destroy();

		if(isDefined(player.defuseicon))
			player.defuseicon destroy();

		if(isDefined(player.progressbackground))
			player.progressbackground destroy();

		if(isDefined(player.progressbar))
			player.progressbar destroy();

		player unlink();
		player enableWeapon();
	}
	
	// cashmodcode
	thread cm_updateLoseRoundCounts(roundwinner);	

	objective_delete(0);
	objective_delete(1);

	if(roundwinner == "allies")
	{
		players = getentarray("player", "classname");
		for(i = 0; i < players.size; i++)
			players[i] playLocalSound("MP_announcer_allies_win");
	}
	else if(roundwinner == "axis")
	{
		players = getentarray("player", "classname");
		for(i = 0; i < players.size; i++)
			players[i] playLocalSound("MP_announcer_axis_win");
	}
	else if(roundwinner == "draw")
	{
		players = getentarray("player", "classname");
		for(i = 0; i < players.size; i++)
			players[i] playLocalSound("MP_announcer_round_draw");
	}
	

	wait 2;

	winners = "";
	losers = "";

	if(roundwinner == "allies")
	{
		game["alliedscore"]++;
		setTeamScore("allies", game["alliedscore"]);
		
		players = getentarray("player", "classname");
		for(i = 0; i < players.size; i++)
		{
			lpGuid = players[i] getGuid();
			if((isdefined(players[i].pers["team"])) && (players[i].pers["team"] == "allies"))
				winners = (winners + ";" + lpGuid + ";" + players[i].name);
			else if((isdefined(players[i].pers["team"])) && (players[i].pers["team"] == "axis"))
				losers = (losers + ";" + lpGuid + ";" + players[i].name);
		}
		logPrint("W;allies" + winners + "\n");
		logPrint("L;axis" + losers + "\n");
	}
	else if(roundwinner == "axis")
	{
		game["axisscore"]++;
		setTeamScore("axis", game["axisscore"]);

		players = getentarray("player", "classname");
		for(i = 0; i < players.size; i++)
		{
			lpGuid = players[i] getGuid();
			if((isdefined(players[i].pers["team"])) && (players[i].pers["team"] == "axis"))
				winners = (winners + ";" + lpGuid + ";" + players[i].name);
			else if((isdefined(players[i].pers["team"])) && (players[i].pers["team"] == "allies"))
				losers = (losers + ";" + lpGuid + ";" + players[i].name);
		}
		logPrint("W;axis" + winners + "\n");
		logPrint("L;allies" + losers + "\n");
	}
	
	if(game["matchstarted"])
	{
		checkScoreLimit();
		game["roundsplayed"]++;
		checkRoundLimit();
		
		// cashmodcode
		cm_checkForHalfTime();
	}

	if(!game["matchstarted"] && roundwinner == "reset")
	{
		game["matchstarted"] = true;
		thread resetScores();
		game["roundsplayed"] = 0;
		cm_setupGameVariables();
	}

	game["timepassed"] = game["timepassed"] + ((getTime() - level.starttime) / 1000) / 60.0;

	checkTimeLimit();

	if(level.mapended)
		return;
	level.mapended = true;

	
	// Store every player's weapons
	players = getentarray("player", "classname");
	for(i = 0; i < players.size; i++)
	{
		player = players[i];
		
		player thread cm_storePlayerWeapons();
	}
	
	wait 3;

	if ( (level.teambalance > 0) && (game["BalanceTeamsNextRound"]) )
	{
		level.lockteams = true;
		level thread maps\mp\gametypes\_teams::TeamBalance();
		level waittill ("Teams Balanced");
		wait 4;
	}
	
	map_restart(true);
}

endMap()
{
	game["state"] = "intermission";
	level notify("intermission");
	
	if(isdefined(level.bombmodel))
		level.bombmodel stopLoopSound();

	if(game["alliedscore"] == game["axisscore"])
		text = &"MPSCRIPT_THE_GAME_IS_A_TIE";
	else if(game["alliedscore"] > game["axisscore"])
		text = &"MPSCRIPT_ALLIES_WIN";
	else
		text = &"MPSCRIPT_AXIS_WIN";

	players = getentarray("player", "classname");
	for(i = 0; i < players.size; i++)
	{
		player = players[i];

		player closeMenu();
		player setClientCvar("g_scriptMainMenu", "main");
		player setClientCvar("cg_objectiveText", text);
		player spawnIntermission();
	}

	wait 10;
	exitLevel(false);
}










checkTimeLimit()
{
	if(level.timelimit <= 0)
		return;
	
	if(game["timepassed"] < level.timelimit)
		return;
	
	if(level.mapended)
		return;
	level.mapended = true;

	iprintln(&"MPSCRIPT_TIME_LIMIT_REACHED");
	level thread endMap();
}

checkScoreLimit()
{
	if(level.scorelimit <= 0)
		return;
	
	if(game["alliedscore"] < level.scorelimit && game["axisscore"] < level.scorelimit)
		return;

	if(level.mapended)
		return;
	level.mapended = true;

	iprintln(&"MPSCRIPT_SCORE_LIMIT_REACHED");
	level thread endMap();
}

checkRoundLimit()
{
	if(level.roundlimit <= 0)
		return;
	
	if(game["roundsplayed"] < level.roundlimit)
		return;
	
	if(level.mapended)
		return;
	level.mapended = true;

	iprintln(&"MPSCRIPT_ROUND_LIMIT_REACHED");
	level thread endMap();
}








updateGametypeCvars()
{
	for(;;)
	{
		timelimit = getCvarFloat("scr_sd_timelimit");
		if(level.timelimit != timelimit)
		{
			if(timelimit > 1440)
			{
				timelimit = 1440;
				setCvar("scr_sd_timelimit", "1440");
			}

			level.timelimit = timelimit;
			setCvar("ui_sd_timelimit", level.timelimit);
		}

		scorelimit = getCvarInt("scr_sd_scorelimit");
		if(level.scorelimit != scorelimit)
		{
			level.scorelimit = scorelimit;
			setCvar("ui_sd_scorelimit", level.scorelimit);

			if(game["matchstarted"])
				checkScoreLimit();
		}

		roundlimit = getCvarInt("scr_sd_roundlimit");
		if(level.roundlimit != roundlimit)
		{
			level.roundlimit = roundlimit;
			setCvar("ui_sd_roundlimit", level.roundlimit);

			if(game["matchstarted"])
				checkRoundLimit();
		}

		roundlength = getCvarFloat("scr_sd_roundlength");
		if(roundlength > 10)
			setCvar("scr_sd_roundlength", "10");

		graceperiod = getCvarFloat("scr_sd_graceperiod");
		if(graceperiod > 60)
			setCvar("scr_sd_graceperiod", "60");

		drawfriend = getCvarFloat("scr_drawfriend");
		if(level.drawfriend != drawfriend)
		{
			level.drawfriend = drawfriend;
			
			if(level.drawfriend)
			{
				// for all living players, show the appropriate headicon
				players = getentarray("player", "classname");
				for(i = 0; i < players.size; i++)
				{
					player = players[i];
					
					if(isDefined(player.pers["team"]) && player.pers["team"] != "spectator" && player.sessionstate == "playing")
					{
						if(player.pers["team"] == "allies")
						{
							player.headicon = game["headicon_allies"];
							player.headiconteam = "allies";
						}
						else
						{
							player.headicon = game["headicon_axis"];
							player.headiconteam = "axis";
						}
					}
				}
			}
			else
			{
				players = getentarray("player", "classname");
				for(i = 0; i < players.size; i++)
				{
					player = players[i];
					
					if(isDefined(player.pers["team"]) && player.pers["team"] != "spectator" && player.sessionstate == "playing")
						player.headicon = "";
				}
			}
		}

		killcam = getCvarInt("scr_killcam");
		if (level.killcam != killcam)
		{
			level.killcam = getCvarInt("scr_killcam");
			if(level.killcam >= 1)
				setarchive(true);
			else
				setarchive(false);
		}
		
		freelook = getCvarInt("scr_freelook");
		if (level.allowfreelook != freelook)
		{
			level.allowfreelook = getCvarInt("scr_freelook");
			level maps\mp\gametypes\_teams::UpdateSpectatePermissions();
		}
		
		enemyspectate = getCvarInt("scr_spectateenemy");
		if (level.allowenemyspectate != enemyspectate)
		{
			level.allowenemyspectate = getCvarInt("scr_spectateenemy");
			level maps\mp\gametypes\_teams::UpdateSpectatePermissions();
		}
		
		teambalance = getCvarInt("scr_teambalance");
		if (level.teambalance != teambalance)
		{
			level.teambalance = getCvarInt("scr_teambalance");
			if (level.teambalance > 0)
				level thread maps\mp\gametypes\_teams::TeamBalance_Check_Roundbased();
		}

		wait 1;
	}
}

updateTeamStatus()
{
	wait 0;	// Required for Callback_PlayerDisconnect to complete before updateTeamStatus can execute
	
	resettimeout();
	
	oldvalue["allies"] = level.exist["allies"];
	oldvalue["axis"] = level.exist["axis"];
	level.exist["allies"] = 0;
	level.exist["axis"] = 0;
	
	players = getentarray("player", "classname");
	for(i = 0; i < players.size; i++)
	{
		player = players[i];
		
		if(isDefined(player.pers["team"]) && player.pers["team"] != "spectator" && player.sessionstate == "playing")
			level.exist[player.pers["team"]]++;
	}

	if(level.exist["allies"])
		level.didexist["allies"] = true;
	if(level.exist["axis"])
		level.didexist["axis"] = true;

	if(level.roundended)
		return;

	if(oldvalue["allies"] && !level.exist["allies"] && oldvalue["axis"] && !level.exist["axis"])
	{
		if(!level.bombplanted)
		{
			announcement(&"SD_ROUNDDRAW");
			level thread endRound("draw");
			return;
		}

		if(game["attackers"] == "allies")
		{
			announcement(&"SD_ALLIEDMISSIONACCOMPLISHED");
			level thread endRound("allies");
			return;
		}

		announcement(&"SD_AXISMISSIONACCOMPLISHED");
		level thread endRound("axis");
		return;
	}

	if(oldvalue["allies"] && !level.exist["allies"])
	{
		// no bomb planted, axis win
		if(!level.bombplanted)
		{
			announcement(&"SD_ALLIESHAVEBEENELIMINATED");
			level thread endRound("axis");
			return;
		}

		if(game["attackers"] == "allies")
			return;
		
		// allies just died and axis have planted the bomb
		if(level.exist["axis"])
		{
			announcement(&"SD_ALLIESHAVEBEENELIMINATED");
			level thread endRound("axis");
			return;
		}

		announcement(&"SD_AXISMISSIONACCOMPLISHED");
		level thread endRound("axis");
		return;
	}
	
	if(oldvalue["axis"] && !level.exist["axis"])
	{
		// no bomb planted, allies win
		if(!level.bombplanted)
		{
			announcement(&"SD_AXISHAVEBEENELIMINATED");
			level thread endRound("allies");
			return;
 		}
 		
 		if(game["attackers"] == "axis")
			return;
		
		// axis just died and allies have planted the bomb
		if(level.exist["allies"])
		{
			announcement(&"SD_AXISHAVEBEENELIMINATED");
			level thread endRound("allies");
			return;
		}
		
		announcement(&"SD_ALLIEDMISSIONACCOMPLISHED");
		level thread endRound("allies");
		return;
	}	
}

resetScores()
{
	players = getentarray("player", "classname");
	for(i = 0; i < players.size; i++)
	{
		player = players[i];
		player.pers["score"] = 0;
		player.pers["deaths"] = 0;
	}

	game["alliedscore"] = 0;
	setTeamScore("allies", game["alliedscore"]);
	game["axisscore"] = 0;
	setTeamScore("axis", game["axisscore"]);
}








Callback_PlayerDisconnect()
{
	iprintln(&"MPSCRIPT_DISCONNECTED", self);
	
	lpselfnum = self getEntityNumber();
	lpselfguid = self getGuid();
	logPrint("Q;" + lpselfguid + ";" + lpselfnum + ";" + self.name + "\n");

	if(game["matchstarted"])
		level thread updateTeamStatus();
		
	// cashmodcode
	cm_clearPlayerWeapons();
	cm_clearPlayerMoney();
}

Callback_PlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc)
{
	if(self.sessionteam == "spectator")
		return;

	// Don't do knockback if the damage direction was not specified
	if(!isDefined(vDir))
		iDFlags |= level.iDFLAGS_NO_KNOCKBACK;

	// check for completely getting out of the damage
	if(!(iDFlags & level.iDFLAGS_NO_PROTECTION))
	{
		if(isPlayer(eAttacker) && (self != eAttacker) && (self.pers["team"] == eAttacker.pers["team"]))
		{
			if(level.friendlyfire == "0")
			{
				return;
			}
			else if(level.friendlyfire == "1")
			{
				// Make sure at least one point of damage is done
				if(iDamage < 1)
					iDamage = 1;

				self finishPlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc);
			}
			else if(level.friendlyfire == "2")
			{
				eAttacker.friendlydamage = true;
		
				iDamage = iDamage * .5;

				// Make sure at least one point of damage is done
				if(iDamage < 1)
					iDamage = 1;

				eAttacker finishPlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc);
				eAttacker.friendlydamage = undefined;
				
				friendly = true;
			}
			else if(level.friendlyfire == "3")
			{
				eAttacker.friendlydamage = true;

				iDamage = iDamage * .5;

				// Make sure at least one point of damage is done
				if(iDamage < 1)
					iDamage = 1;

				self finishPlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc);
				eAttacker finishPlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc);
				eAttacker.friendlydamage = undefined;
				
				friendly = true;
			}
		}
		else
		{
			// Make sure at least one point of damage is done
			if(iDamage < 1)
				iDamage = 1;

			self finishPlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc);
		}
	}

	// Do debug print if it's enabled
	if(getCvarInt("g_debugDamage"))
	{
		println("client:" + self getEntityNumber() + " health:" + self.health +
			" damage:" + iDamage + " hitLoc:" + sHitLoc);
	}

	if(self.sessionstate != "dead")
	{
		lpselfnum = self getEntityNumber();
		lpselfguid = self getGuid();
		lpselfname = self.name;
		lpselfteam = self.pers["team"];
		lpattackerteam = "";

		if(isPlayer(eAttacker))
		{
			lpattacknum = eAttacker getEntityNumber();
			lpattackguid = self getGuid();
			lpattackname = eAttacker.name;
			lpattackerteam = eAttacker.pers["team"];
		}
		else
		{
			lpattacknum = -1;
			lpattackguid = "";
			lpattackname = "";
			lpattackerteam = "world";
		}

		if(isDefined(friendly))
		{  
			lpattacknum = lpselfnum;
			lpattackname = lpselfname;
			lpattackguid = lpselfguid;
		}

		logPrint("D;" + lpselfguid + ";" + lpselfnum + ";" + lpselfteam + ";" + lpselfname + ";" + lpattackguid + ";" + lpattacknum + ";" + lpattackerteam + ";" + lpattackname + ";" + sWeapon + ";" + iDamage + ";" + sMeansOfDeath + ";" + sHitLoc + "\n");
	}
}









spawnIntermission()
{
	self notify("spawned");
	
	resettimeout();

	self.sessionstate = "intermission";
	self.spectatorclient = -1;
	self.archivetime = 0;
	self.friendlydamage = undefined;

	spawnpointname = "mp_searchanddestroy_intermission";
	spawnpoints = getentarray(spawnpointname, "classname");
	spawnpoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_Random(spawnpoints);

	if(isDefined(spawnpoint))
		self spawn(spawnpoint.origin, spawnpoint.angles);
	else
		maps\mp\_utility::error("NO " + spawnpointname + " SPAWNPOINTS IN MAP");
}

killcam(attackerNum, delay)
{
	self endon("spawned");
	
	// killcam
	if(attackerNum < 0)
		return;

	self.sessionstate = "spectator";
	self.spectatorclient = attackerNum;
	self.archivetime = delay + 7;

	maps\mp\gametypes\_teams::SetKillcamSpectatePermissions();

	// wait till the next server frame to allow code a chance to update archivetime if it needs trimming
	wait 0.05;

	if(self.archivetime <= delay)
	{
		self.spectatorclient = -1;
		self.archivetime = 0;
	
		maps\mp\gametypes\_teams::SetSpectatePermissions();
		return;
	}

	self.killcam = true;

	if(!isDefined(self.kc_topbar))
	{
		self.kc_topbar = newClientHudElem(self);
		self.kc_topbar.archived = false;
		self.kc_topbar.x = 0;
		self.kc_topbar.y = 0;
		self.kc_topbar.alpha = 0.5;
		self.kc_topbar setShader("black", 640, 112);
	}

	if(!isDefined(self.kc_bottombar))
	{
		self.kc_bottombar = newClientHudElem(self);
		self.kc_bottombar.archived = false;
		self.kc_bottombar.x = 0;
		self.kc_bottombar.y = 368;
		self.kc_bottombar.alpha = 0.5;
		self.kc_bottombar setShader("black", 640, 112);
	}

	if(!isDefined(self.kc_title))
	{
		self.kc_title = newClientHudElem(self);
		self.kc_title.archived = false;
		self.kc_title.x = 320;
		self.kc_title.y = 40;
		self.kc_title.alignX = "center";
		self.kc_title.alignY = "middle";
		self.kc_title.sort = 1; // force to draw after the bars
		self.kc_title.fontScale = 3.5;
	}
	self.kc_title setText(&"MPSCRIPT_KILLCAM");

	if(!isDefined(self.kc_skiptext))
	{
		self.kc_skiptext = newClientHudElem(self);
		self.kc_skiptext.archived = false;
		self.kc_skiptext.x = 320;
		self.kc_skiptext.y = 70;
		self.kc_skiptext.alignX = "center";
		self.kc_skiptext.alignY = "middle";
		self.kc_skiptext.sort = 1; // force to draw after the bars
	}
	self.kc_skiptext setText(&"MPSCRIPT_PRESS_ACTIVATE_TO_SKIP");

	if(!isDefined(self.kc_timer))
	{
		self.kc_timer = newClientHudElem(self);
		self.kc_timer.archived = false;
		self.kc_timer.x = 320;
		self.kc_timer.y = 428;
		self.kc_timer.alignX = "center";
		self.kc_timer.alignY = "middle";
		self.kc_timer.fontScale = 3.5;
		self.kc_timer.sort = 1;
	}
	self.kc_timer setTenthsTimer(self.archivetime - delay);

	self thread spawnedKillcamCleanup();
	self thread waitSkipKillcamButton();
	self thread waitKillcamTime();
	self waittill("end_killcam");

	self removeKillcamElements();

	self.spectatorclient = -1;
	self.archivetime = 0;
	self.killcam = undefined;
	
	maps\mp\gametypes\_teams::SetSpectatePermissions();
}

waitKillcamTime()
{
	self endon("end_killcam");
	
	wait(self.archivetime - 0.05);
	self notify("end_killcam");
}

waitSkipKillcamButton()
{
	self endon("end_killcam");
	
	while(self useButtonPressed())
		wait .05;

	while(!(self useButtonPressed()))
		wait .05;
	
	self notify("end_killcam");	
}

removeKillcamElements()
{
	if(isDefined(self.kc_topbar))
		self.kc_topbar destroy();
	if(isDefined(self.kc_bottombar))
		self.kc_bottombar destroy();
	if(isDefined(self.kc_title))
		self.kc_title destroy();
	if(isDefined(self.kc_skiptext))
		self.kc_skiptext destroy();
	if(isDefined(self.kc_timer))
		self.kc_timer destroy();
}

spawnedKillcamCleanup()
{
	self endon("end_killcam");

	self waittill("spawned");
	self removeKillcamElements();
}









bombzones()
{
	level.barsize = 288;
	level.planttime = 5;		// seconds to plant a bomb
	level.defusetime = 10;		// seconds to defuse a bomb

	bombtrigger = getent("bombtrigger", "targetname");
	bombtrigger maps\mp\_utility::triggerOff();

	bombzone_A = getent("bombzone_A", "targetname");
	bombzone_B = getent("bombzone_B", "targetname");
	bombzone_A thread bombzone_think(bombzone_B);
	bombzone_B thread bombzone_think(bombzone_A);

	wait 1;	// TEMP: without this one of the objective icon is the default. Carl says we're overflowing something.
	objective_add(0, "current", bombzone_A.origin, "gfx/hud/hud@objectiveA.tga");
	objective_add(1, "current", bombzone_B.origin, "gfx/hud/hud@objectiveB.tga");
}

bombzone_think(bombzone_other)
{
	level endon("round_ended");

	level.barincrement = (level.barsize / (20.0 * level.planttime));
	
	for(;;)
	{
		self waittill("trigger", other);

		if(isDefined(bombzone_other.planting))
		{
			if(isDefined(other.planticon))
				other.planticon destroy();

			continue;
		}
		
		if(isPlayer(other) && (other.pers["team"] == game["attackers"]) && other isOnGround())
		{
			if(!isDefined(other.planticon))
			{
				other.planticon = newClientHudElem(other);				
				other.planticon.alignX = "center";
				other.planticon.alignY = "middle";
				other.planticon.x = 320;
				other.planticon.y = 345;
				other.planticon setShader("ui_mp/assets/hud@plantbomb.tga", 64, 64);			
			}
			
			while(other istouching(self) && isAlive(other) && other useButtonPressed())
			{
				other notify("kill_check_bombzone");
				
				self.planting = true;

				if(!isDefined(other.progressbackground))
				{
					other.progressbackground = newClientHudElem(other);				
					other.progressbackground.alignX = "center";
					other.progressbackground.alignY = "middle";
					other.progressbackground.x = 320;
					other.progressbackground.y = 385;
					other.progressbackground.alpha = 0.5;
				}
				other.progressbackground setShader("black", (level.barsize + 4), 12);		

				if(!isDefined(other.progressbar))
				{
					other.progressbar = newClientHudElem(other);				
					other.progressbar.alignX = "left";
					other.progressbar.alignY = "middle";
					other.progressbar.x = (320 - (level.barsize / 2.0));
					other.progressbar.y = 385;
				}
				other.progressbar setShader("white", 0, 8);
				other.progressbar scaleOverTime(level.planttime, level.barsize, 8);

				other playsound("MP_bomb_plant");
				other linkTo(self);
				other disableWeapon();

				self.progresstime = 0;
				while(isAlive(other) && other useButtonPressed() && (self.progresstime < level.planttime))
				{
					self.progresstime += 0.05;
					wait 0.05;
				}
	
				if(isDefined(other.progressbackground))
					other.progressbackground destroy();
				if(isDefined(other.progressbar))
					other.progressbar destroy();

				if(self.progresstime >= level.planttime)
				{
					if(isDefined(other.planticon))
						other.planticon destroy();

					other enableWeapon();

					level.bombexploder = self.script_noteworthy;
					
					bombzone_A = getent("bombzone_A", "targetname");
					bombzone_B = getent("bombzone_B", "targetname");
					bombzone_A delete();
					bombzone_B delete();
					objective_delete(0);
					objective_delete(1);
	
					plant = other maps\mp\_utility::getPlant();
					
					level.bombmodel = spawn("script_model", plant.origin);
					level.bombmodel.angles = plant.angles;
					level.bombmodel setmodel("xmodel/mp_bomb1_defuse");
					level.bombmodel playSound("Explo_plant_no_tick");
					
					bombtrigger = getent("bombtrigger", "targetname");
					bombtrigger.origin = level.bombmodel.origin;

					objective_add(0, "current", bombtrigger.origin, "gfx/hud/hud@bombplanted.tga");
		
					level.bombplanted = true;
					
					lpselfnum = other getEntityNumber();
					lpselfguid = other getGuid();
					logPrint("A;" + lpselfguid + ";" + lpselfnum + ";" + game["attackers"] + ";" + other.name + ";" + "bomb_plant" + "\n");
					
					announcement(&"SD_EXPLOSIVESPLANTED");
										
					players = getentarray("player", "classname");
					for(i = 0; i < players.size; i++)
						players[i] playLocalSound("MP_announcer_bomb_planted");
					
					bombtrigger thread bomb_think();
					bombtrigger thread bomb_countdown();
					
					level notify("bomb_planted");
					level.clock destroy();
					
					
					// cashmodcode
					game["bombdown"] = 1;
					
					return;	//TEMP, script should stop after the wait .05
				}
				else
				{
					other unlink();
					other enableWeapon();
				}
				
				wait .05;
			}
			
			self.planting = undefined;
			other thread check_bombzone(self);
		}
	}
}

check_bombzone(trigger)
{
	self notify("kill_check_bombzone");
	self endon("kill_check_bombzone");
	level endon("round_ended");

	while(isDefined(trigger) && !isDefined(trigger.planting) && self istouching(trigger) && isAlive(self))
		wait 0.05;

	if(isDefined(self.planticon))
		self.planticon destroy();
}

bomb_countdown()
{
	self endon("bomb_defused");
	level endon("intermission");
	
	level.bombmodel playLoopSound("bomb_tick");
	
	// set the countdown time
	countdowntime = 60;

	wait countdowntime;
		
	// bomb timer is up
	objective_delete(0);
	
	level.bombexploded = true;
	self notify("bomb_exploded");

	// trigger exploder if it exists
	if(isDefined(level.bombexploder))
		maps\mp\_utility::exploder(level.bombexploder);

	// explode bomb
	origin = self getorigin();
	range = 500;
	maxdamage = 2000;
	mindamage = 1000;
		
	self delete(); // delete the defuse trigger
	level.bombmodel stopLoopSound();
	level.bombmodel delete();

	playfx(level._effect["bombexplosion"], origin);
	radiusDamage(origin, range, maxdamage, mindamage);
	
	level thread endRound(game["attackers"]);
}

bomb_think()
{
	self endon("bomb_exploded");
	level.barincrement = (level.barsize / (20.0 * level.defusetime));

	for(;;)
	{
		self waittill("trigger", other);
		
		// check for having been triggered by a valid player
		if(isPlayer(other) && (other.pers["team"] == game["defenders"]) && other isOnGround())
		{
			if(!isDefined(other.defuseicon))
			{
				other.defuseicon = newClientHudElem(other);				
				other.defuseicon.alignX = "center";
				other.defuseicon.alignY = "middle";
				other.defuseicon.x = 320;
				other.defuseicon.y = 345;
				other.defuseicon setShader("ui_mp/assets/hud@defusebomb.tga", 64, 64);			
			}
			
			while(other islookingat(self) && distance(other.origin, self.origin) < 64 && isAlive(other) && other useButtonPressed())
			{
				other notify("kill_check_bomb");

				if(!isDefined(other.progressbackground))
				{
					other.progressbackground = newClientHudElem(other);				
					other.progressbackground.alignX = "center";
					other.progressbackground.alignY = "middle";
					other.progressbackground.x = 320;
					other.progressbackground.y = 385;
					other.progressbackground.alpha = 0.5;
				}
				other.progressbackground setShader("black", (level.barsize + 4), 12);		

				if(!isDefined(other.progressbar))
				{
					other.progressbar = newClientHudElem(other);				
					other.progressbar.alignX = "left";
					other.progressbar.alignY = "middle";
					other.progressbar.x = (320 - (level.barsize / 2.0));
					other.progressbar.y = 385;
				}
				other.progressbar setShader("white", 0, 8);			
				other.progressbar scaleOverTime(level.defusetime, level.barsize, 8);

				other playsound("MP_bomb_defuse");
				other linkTo(self);
				other disableWeapon();

				self.progresstime = 0;
				while(isAlive(other) && other useButtonPressed() && (self.progresstime < level.defusetime))
				{
					self.progresstime += 0.05;
					wait 0.05;
				}

				if(isDefined(other.progressbackground))
					other.progressbackground destroy();
				if(isDefined(other.progressbar))
					other.progressbar destroy();

				if(self.progresstime >= level.defusetime)
				{
					if(isDefined(other.defuseicon))
						other.defuseicon destroy();

					objective_delete(0);

					self notify("bomb_defused");
					level.bombmodel setmodel("xmodel/mp_bomb1");
					level.bombmodel stopLoopSound();
					self delete();

					announcement(&"SD_EXPLOSIVESDEFUSED");
					
					lpselfnum = other getEntityNumber();
					lpselfguid = other getGuid();
					logPrint("A;" + lpselfguid + ";" + lpselfnum + ";" + game["defenders"] + ";" + other.name + ";" + "bomb_defuse" + "\n");
					
					players = getentarray("player", "classname");
					for(i = 0; i < players.size; i++)
						players[i] playLocalSound("MP_announcer_bomb_defused");

					level thread endRound(game["defenders"]);
					game["bombdown"] = 0;
					return;	//TEMP, script should stop after the wait .05
				}
				else
				{
					other unlink();
					other enableWeapon();
				}
				
				wait .05;
			}

			self.defusing = undefined;
			other thread check_bomb(self);
		}
	}
}

check_bomb(trigger)
{
	self notify("kill_check_bomb");
	self endon("kill_check_bomb");

	while(isDefined(trigger) && !isDefined(trigger.defusing) && distance(self.origin, trigger.origin) < 32 && self islookingat(trigger) && isAlive(self))
		wait 0.05;

	if(isDefined(self.defuseicon))
		self.defuseicon destroy();
}






printJoinedTeam(team)
{
	if(team == "allies")
		iprintln(&"MPSCRIPT_JOINED_ALLIES", self);
	else if(team == "axis")
		iprintln(&"MPSCRIPT_JOINED_AXIS", self);
}

addBotClients()
{
	wait 5;
	
	for(i = 0; i < 2; i++)
	{
		ent[i] = addtestclient();
		wait 0.5;
	
		if(isPlayer(ent[i]))
		{
			if(i & 1)
			{
				ent[i] notify("menuresponse", game["menu_team"], "axis");
				wait 0.5;
				ent[i] notify("menuresponse", game["menu_weapon_axis"], "kar98k_mp");
			}
			else
			{
				ent[i] notify("menuresponse", game["menu_team"], "allies");
				wait 0.5;
				ent[i] notify("menuresponse", game["menu_weapon_allies"], "m1garand_mp");
			}
		}
	}
}


















// CASHMODFUNCTIONS


// Player Money ================================================================================//

cm_updateMoney(reason, item, amount)
{
	self.pers["money"] += amount;
	
	if (self.pers["money"] > game["maxplayermoney"])
		self.pers["money"] = game["maxplayermoney"];
	
	if (amount == 0)
		return;
		
	self thread cm_updateHud();
	self thread cm_printMoneyChange(reason, item, amount);
}

cm_endRoundMoney(winner)
{
	if (self.pers["team"] == winner)
	{
		if (self.pers["team"] == "allies")
		{
			// The attacking team gets a bonus for winning the round
			// with the bomb planted
			if (game["bombdown"] == 1)
			{
				amount = cm_getMoneyReward("allies_winroundplant");
			}
			else
			{
				amount = cm_getMoneyReward("allies_winrounddefault");
			}
		}
		else
		{
			// The defending team gets a static winning amount regardless
			amount = cm_getMoneyReward("axis_winround");
		}
		
		self cm_updateMoney("reward", "winround", amount);
	}
	
	else
	{
		base = cm_getMoneyReward("loseroundbase");
		bonus = cm_getMoneyReward("loseroundbonus");
		if (self.pers["team"] == "allies")
			loseRoundCount = game["allies_loseroundcount"];
		else
			loseRoundCount = game["axis_loseroundcount"];
			
		// The losing team each get a base amount plus a bonus
		// that increases for every consecutive round lost up to 5 times
		// and decreases for every round won
		amount = base + (bonus * loseRoundCount);
		
		self cm_updateMoney("reward", "loseround", amount);
	}
}

cm_setupPlayerMoney()
{
	self.pers["money"] = game["startplayermoney"];
}

cm_clearPlayerMoney()
{
	self.pers["money"] = undefined;
	self.moneyHud destroy();
}








// Player Hud =================================================================================//

cm_updateHud()
{		
	if (!isDefined(self.moneyHud))
	{
		self.moneyHud = newClientHudElem(self);
		self.moneyHud.x = 630;
		self.moneyHud.y = 400;
		self.moneyHud.alignX = "right";
	}
	
	self.moneyHud setValue(self.pers["money"]);
}

cm_printMoneyChange(reason, item, amount)
{
	wait(0.1);
	
	if (reason == "reward")
	{
		switch (item)
		{
			case "frag" :
				self iprintln("^3$$  +" + amount + "  [Kill]");
				break;
			
			case "winround" :
				self iprintln("^3$$  +" + amount + "  [Round won]");
				break;
			
			case "loseround" :
				if (self.pers["team"] == "allies")
					loseCount = game["allies_loseroundcount"];
				else
					loseCount = game["axis_loseroundcount"];
				self iprintln("^3$$  +" + amount + "  [Round lost][" + loseCount + " consecutive]");
				break;
			
			case "teamkill" :
				self iprintln("^3$$  " + amount + "  [Team kill]");
				break;
			
			default :
				break;
		}
	}
	
	else
	{
		itemName = maps\mp\gametypes\_teams::getWeaponName(item);
		
		switch (reason)
		{
			case "buyweapon" :
				if (item == self getWeaponSlotWeapon("primary"))
					self iprintln("^3$$  " + amount + "  [Primary weapon]");
				else
					self iprintln("^3$$  " + amount + "  [Secondary weapon]");
				break;
			
			case "buyprimarymagazine" :
				self iprintln("^3$$  " + amount + "  [Primary magazine]");
				break;
			
			case "buysecondarymagazine" :
				self iprintln("^3$$  " + amount + "  [Secondary magazine]");
				break;
			
			case "buypistolmagazine" :
				self iprintln("^3$$  " + amount + "  [Pistol magazine]");
				break;
			
			case "buygrenade" :
				self iprintln("^3$$  " + amount + "  [Grenade]");
				break;
			
			
			default :
				break;
		}
	}
}

// Deprecated
cm_scrollMoneyChangeHud(amount)
{
	if (!isDefined (self.scrollHud1) )
	{
		self.scrollHud1 = newClientHudElem(self);
		self.scrollHud1 setValue(amount);
		
		self.scrollHud1.x = 630;
		self.scrollHud1.y = 380;
		self.scrollHud1.alignX = "right";
		
		self.scrollHud1 moveOverTime(3);
		self.scrollHud1 fadeOverTime(3);
		
		self.scrollHud1.x = 630;
		self.scrollHud1.y = 300;
		self.scrollHud1.alpha = 0;
		
		wait(3);
		
		self.scrollHud1 destroy();
	}
	
	else if (!isDefined (self.scrollHud2) )
	{
		self.scrollHud2 = newClientHudElem(self);
		self.scrollHud2 setValue(amount);
		
		self.scrollHud2.x = 630;
		self.scrollHud2.y = 380;
		self.scrollHud2.alignX = "right";
		
		self.scrollHud2 moveOverTime(3);
		self.scrollHud2 fadeOverTime(3);
		
		self.scrollHud2.x = 630;
		self.scrollHud2.y = 300;
		self.scrollHud2.alpha = 0;
		
		wait(3);
		
		self.scrollHud2 destroy();
	}
	
	else if (!isDefined (self.scrollHud3) )
	{
		self.scrollHud3 = newClientHudElem(self);
		self.scrollHud3 setValue(amount);
		
		self.scrollHud3.x = 630;
		self.scrollHud3.y = 380;
		self.scrollHud3.alignX = "right";
		
		self.scrollHud3 moveOverTime(3);
		self.scrollHud3 fadeOverTime(3);
		
		self.scrollHud3.x = 630;
		self.scrollHud3.y = 300;
		self.scrollHud3.alpha = 0;
		
		wait(3);
		
		self.scrollHud3 destroy();
	}
	
	else if (!isDefined (self.scrollHud4) )
	{
		self.scrollHud4 = newClientHudElem(self);
		self.scrollHud4 setValue(amount);
		
		self.scrollHud4.x = 630;
		self.scrollHud4.y = 380;
		self.scrollHud4.alignX = "right";
		
		self.scrollHud4 moveOverTime(3);
		self.scrollHud4 fadeOverTime(3);
		
		self.scrollHud4.x = 630;
		self.scrollHud4.y = 300;
		self.scrollHud4.alpha = 0;
		
		wait(3);
		
		self.scrollHud4 destroy();
	}
	
	else if (!isDefined (self.scrollHud5) )
	{
		self.scrollHud5 = newClientHudElem(self);
		self.scrollHud5 setValue(amount);
		
		self.scrollHud5.x = 630;
		self.scrollHud5.y = 380;
		self.scrollHud5.alignX = "right";
		
		self.scrollHud5 moveOverTime(3);
		self.scrollHud5 fadeOverTime(3);
		
		self.scrollHud5.x = 630;
		self.scrollHud5.y = 300;
		self.scrollHud5.alpha = 0;
		
		wait(3);
		
		self.scrollHud5 destroy();
	}
	
	else if (!isDefined (self.scrollHud6) )
	{
		self.scrollHud6 = newClientHudElem(self);
		self.scrollHud6 setValue(amount);
		
		self.scrollHud6.x = 630;
		self.scrollHud6.y = 380;
		self.scrollHud6.alignX = "right";
		
		self.scrollHud6 moveOverTime(3);
		self.scrollHud6 fadeOverTime(3);
		
		self.scrollHud6.x = 630;
		self.scrollHud6.y = 300;
		self.scrollHud6.alpha = 0;
		
		wait(3);
		
		self.scrollHud6 destroy();
	}
	
	else if (!isDefined (self.scrollHud7) )
	{
		self.scrollHud7 = newClientHudElem(self);
		self.scrollHud7 setValue(amount);
		
		self.scrollHud7.x = 630;
		self.scrollHud7.y = 380;
		self.scrollHud7.alignX = "right";
		
		self.scrollHud7 moveOverTime(3);
		self.scrollHud7 fadeOverTime(3);
		
		self.scrollHud7.x = 630;
		self.scrollHud7.y = 300;
		self.scrollHud7.alpha = 0;
		
		wait(3);
		
		self.scrollHud7 destroy();
	}
	
	else if (!isDefined (self.scrollHud8) )
	{
		self.scrollHud8 = newClientHudElem(self);
		self.scrollHud8 setValue(amount);
		
		self.scrollHud8.x = 630;
		self.scrollHud8.y = 380;
		self.scrollHud8.alignX = "right";
		
		self.scrollHud8 moveOverTime(3);
		self.scrollHud8 fadeOverTime(3);
		
		self.scrollHud8.x = 630;
		self.scrollHud8.y = 300;
		self.scrollHud8.alpha = 0;
		
		wait(3);
		
		self.scrollHud8 destroy();
	}
	
	else if (!isDefined (self.scrollHud9) )
	{
		self.scrollHud9 = newClientHudElem(self);
		self.scrollHud9 setValue(amount);
		
		self.scrollHud9.x = 630;
		self.scrollHud9.y = 380;
		self.scrollHud9.alignX = "right";
		
		self.scrollHud9 moveOverTime(3);
		self.scrollHud9 fadeOverTime(3);
		
		self.scrollHud9.x = 630;
		self.scrollHud9.y = 300;
		self.scrollHud9.alpha = 0;
		
		wait(3);
		
		self.scrollHud9 destroy();
	}
	
	else if (!isDefined (self.scrollHud10) )
	{
		self.scrollHud10 = newClientHudElem(self);
		self.scrollHud10 setValue(amount);
		
		self.scrollHud10.x = 630;
		self.scrollHud10.y = 380;
		self.scrollHud10.alignX = "right";
		
		self.scrollHud10 moveOverTime(3);
		self.scrollHud10 fadeOverTime(3);
		
		self.scrollHud10.x = 630;
		self.scrollHud10.y = 300;
		self.scrollHud10.alpha = 0;
		
		wait(3);
		
		self.scrollHud10 destroy();
	}
	
	else if (!isDefined (self.scrollHud11) )
	{
		self.scrollHud11 = newClientHudElem(self);
		self.scrollHud11 setValue(amount);
		
		self.scrollHud11.x = 630;
		self.scrollHud11.y = 380;
		self.scrollHud11.alignX = "right";
		
		self.scrollHud11 moveOverTime(3);
		self.scrollHud11 fadeOverTime(3);
		
		self.scrollHud11.x = 630;
		self.scrollHud11.y = 300;
		self.scrollHud11.alpha = 0;
		
		wait(3);
		
		self.scrollHud11 destroy();
	}
	
	else if (!isDefined (self.scrollHud12) )
	{
		self.scrollHud12 = newClientHudElem(self);
		self.scrollHud12 setValue(amount);
		
		self.scrollHud12.x = 630;
		self.scrollHud12.y = 380;
		self.scrollHud12.alignX = "right";
		
		self.scrollHud12 moveOverTime(3);
		self.scrollHud12 fadeOverTime(3);
		
		self.scrollHud12.x = 630;
		self.scrollHud12.y = 300;
		self.scrollHud12.alpha = 0;
		
		wait(3);
		
		self.scrollHud12 destroy();
	}
	
	else if (!isDefined (self.scrollHud13) )
	{
		self.scrollHud13 = newClientHudElem(self);
		self.scrollHud13 setValue(amount);
		
		self.scrollHud13.x = 630;
		self.scrollHud13.y = 380;
		self.scrollHud13.alignX = "right";
		
		self.scrollHud13 moveOverTime(3);
		self.scrollHud13 fadeOverTime(3);
		
		self.scrollHud13.x = 630;
		self.scrollHud13.y = 300;
		self.scrollHud13.alpha = 0;
		
		wait(3);
		
		self.scrollHud13 destroy();
	}
	
	else if (!isDefined (self.scrollHud14) )
	{
		self.scrollHud14 = newClientHudElem(self);
		self.scrollHud14 setValue(amount);
		
		self.scrollHud14.x = 630;
		self.scrollHud14.y = 380;
		self.scrollHud14.alignX = "right";
		
		self.scrollHud14 moveOverTime(3);
		self.scrollHud14 fadeOverTime(3);
		
		self.scrollHud14.x = 630;
		self.scrollHud14.y = 300;
		self.scrollHud14.alpha = 0;
		
		wait(3);
		
		self.scrollHud14 destroy();
	}
	
	else if (!isDefined (self.scrollHud15) )
	{
		self.scrollHud15 = newClientHudElem(self);
		self.scrollHud15 setValue(amount);
		
		self.scrollHud15.x = 630;
		self.scrollHud15.y = 380;
		self.scrollHud15.alignX = "right";
		
		self.scrollHud15 moveOverTime(3);
		self.scrollHud15 fadeOverTime(3);
		
		self.scrollHud15.x = 630;
		self.scrollHud15.y = 300;
		self.scrollHud15.alpha = 0;
		
		wait(3);
		
		self.scrollHud15 destroy();
	}
}







// Buy Weapons ==============================================================================//

cm_buyWeapon(response)
{
	price = cm_getPrice("weapon", response);
	
	// Check if the player has enough money
	if (self.pers["money"] < price)
	{
		iprintln("Insufficient funds");
		return;
	}
	
	self giveWeapon(response);	
	cm_updateMoney("buyweapon", response, -1 * price);
		
	// check which slot the new weapon is going to
	if (self getWeaponSlotWeapon("primary") == response)
		slotForNewWeapon = "primary";
	else if (self getWeaponSlotWeapon("primaryb") == response)
		slotForNewWeapon = "primaryb";
	// If the weapon doesn't go to either slot, let the player know
	// that they already have 2 weapons
	else
	{
		self iprintln("Can't carry any more weapons");
		return;
	}
	
	// set that slot's ammo to 0 initially
	self setWeaponSlotAmmo(slotForNewWeapon, 0);
	
	// update player variables
	if (slotForNewWeapon == "primary")
		self.pers["primary"] = response;
	else
		self.pers["primaryb"] = response;
	
	// change to the new weapon
	self switchToWeapon(response);
}

cm_buyPrimaryMagazine()
{	
	// Determine the player's primary weapon
	weapon = self getWeaponSlotWeapon("primary");	
	
	// Get weapon magazine price
	price = cm_getPrice("magazine", weapon);
	
	// CHECK IF PLAYER HAS ENOUGH MONEY
	if (self.pers["money"] < price)
	{
		self iprintln("Insufficient funds");
		return;
	}		
	
	// CHECK IF PLAYER HAS MAX AMMO
	// If the player has the maximum amount
	// don't let them purchase another magazine
	maxAmmo = cm_getWeaponMaxAmmo(weapon);
	currentAmmo = self getWeaponSlotAmmo("primary");
	if (currentAmmo >= maxAmmo)
	{
		self iprintln("Max ammo for primary weapon");
		return;
	}
	
	// Find the magazine size for the weapon
	magazineSize = cm_getWeaponMagazineSize(weapon);		
	
	// Add one magazine
	// Adding a magazine might send the ammo count over the max
	// if so, limit it
	if ( (magazineSize + currentAmmo) > maxAmmo)
		self setWeaponSlotAmmo("primary", maxAmmo);
	else
		self setWeaponSlotAmmo("primary", magazineSize + currentAmmo);
			
	// Subtract the price from the player's money
	self cm_updateMoney("buyprimarymagazine", weapon, -1 * price);
	
	self switchToWeapon(weapon);
}

cm_buySecondaryMagazine()
{
	// Determine the player's secondary weapon
	weapon = self getWeaponSlotWeapon("primaryb");	
	
	// Get weapon magazine price
	price = cm_getPrice("magazine", weapon);
	
	// CHECK IF PLAYER HAS ENOUGH MONEY
	if (self.pers["money"] < price)
	{
		self iprintln("Insufficient funds");
		return;
	}		
	
	// CHECK IF PLAYER HAS MAX AMMO
	// If the player has the maximum amount
	// don't let them purchase another magazine
	maxAmmo = cm_getWeaponMaxAmmo(weapon);
	currentAmmo = self getWeaponSlotAmmo("primaryb");
	if (currentAmmo >= maxAmmo)
	{
		self iprintln("Max ammo for secondary weapon");
		return;
	}
	
	// Find the magazine size for the weapon
	magazineSize = cm_getWeaponMagazineSize(weapon);		
	
	// Add one magazine
	// Adding a magazine might send the ammo count over the max
	// if so, limit it
	if ( (magazineSize + currentAmmo) > maxAmmo)
		self setWeaponSlotAmmo("primaryb", maxAmmo);
	else
		self setWeaponSlotAmmo("primaryb", magazineSize + currentAmmo);
			
	// Subtract the price from the player's money
	self cm_updateMoney("buysecondarymagazine", weapon, -1 * price);
	
	self switchToWeapon(weapon);
}

cm_buyPistolMagazine()
{
	// Determine the player's pistol
	weapon = self getWeaponSlotWeapon("pistol");	
	
	// Get weapon magazine price
	price = cm_getPrice("magazine", weapon);
	
	// CHECK IF PLAYER HAS ENOUGH MONEY
	if (self.pers["money"] < price)
	{
		self iprintln("Insufficient funds");
		return;
	}		
	
	// CHECK IF PLAYER HAS MAX AMMO
	// If the player has the maximum amount
	// don't let them purchase another magazine
	maxAmmo = cm_getWeaponMaxAmmo(weapon);
	currentAmmo = self getWeaponSlotAmmo("pistol");
	if (currentAmmo >= maxAmmo)
	{
		self iprintln("Max ammo for pistol");
		return;
	}
	
	// Find the magazine size for the weapon
	magazineSize = cm_getWeaponMagazineSize(weapon);		
	
	// Add one magazine
	// Adding a magazine might send the ammo count over the max
	// if so, limit it
	if ( (magazineSize + currentAmmo) > maxAmmo)
		self setWeaponSlotAmmo("pistol", maxAmmo);
	else
		self setWeaponSlotAmmo("pistol", magazineSize + currentAmmo);
			
	// Subtract the price from the player's money
	self cm_updateMoney("buypistolmagazine", weapon, -1 * price);
	
	self switchToWeapon(weapon);
}

cm_buyGrenade()
{
	// Get grenade price
	price = cm_getPrice("grenade", "null");
	
	// CHECK IF PLAYER HAS ENOUGH MONEY
	if (self.pers["money"] < price)
	{
		self iprintln("Insufficient funds");
		return;
	}
	
	// CHECK IF PLAYER HAS MAX AMMO
	// If they do, don't let them purchase more
	maxAmmo = cm_getWeaponMaxAmmo("grenade");
	currentAmmo = self getWeaponSlotAmmo("grenade");
	if (currentAmmo >= maxAmmo)
	{
		self iprintln("Max Grenades");
		return;
	}
	
	// If the player doesn't already have a grenade
	else if (currentAmmo < 1)
	{
		// Give them one
		if (self.pers["team"] == "axis") 
			self giveWeapon("stielhandgranate_mp");
		
		else if (self.pers["team"] == "allies")
		{
			if (game["allies"] == "american")
				self giveWeapon("fraggrenade_mp");
			else if (game["allies"] == "british")
				self giveWeapon("mk1britishfrag_mp");
			else if (game["allies"] == "russian")
				self giveWeapon("rgd-33russianfrag_mp");
		}
		
		// Make sure it's only one
		self setWeaponSlotAmmo("grenade", 1);
	}
	
	else
	{	
		// Add one grenade
		self setWeaponSlotAmmo("grenade", currentAmmo + 1);	
	}
	
	// Get the player's grenade type
	grenade = self getWeaponSlotWeapon("grenade");
	
	// Subtract the price from the player's money
	cm_updateMoney("buygrenade", grenade, -1 * price);
		
	self.pers["grenade"] = grenade;
	self switchToWeapon(grenade);
}







// Weapon Utilities ==============================================================================//

cm_storePlayerWeapons()
{
	// Primary Weapon
	self.pers["primary"] = self getWeaponSlotWeapon("primary");
	self.pers["primarymagazineammo"] = self getWeaponSlotClipAmmo("primary");
	self.pers["primaryspareammo"] = self getWeaponSlotAmmo("primary");	
	
	// Secondary Weapon
	self.pers["primaryb"] = self getWeaponSlotWeapon("primaryb");
	self.pers["primarybmagazineammo"] = self getWeaponSlotClipAmmo("primaryb");
	self.pers["primarybspareammo"] = self getWeaponSlotAmmo("primaryb");	
	
	// Pistol
	self.pers["pistol"] = self getWeaponSlotWeapon("pistol");
	self.pers["pistolmagazineammo"] = self getWeaponSlotClipAmmo("pistol");
	self.pers["pistolspareammo"] = self getWeaponSlotAmmo("pistol");	
	
	// Grenades
	self.pers["grenade"] = self getWeaponSlotWeapon("grenade");
	self.pers["grenadeammo"] = self getWeaponSlotClipAmmo("grenade");	
}

cm_loadPlayerWeapons()
{
	// Primary Weapon
	if (isDefined (self.pers["primary"]) )
	{
		self setWeaponSlotWeapon("primary", self.pers["primary"]);
		self setWeaponSlotClipAmmo("primary", self.pers["primarymagazineammo"]);
		self setWeaponSlotAmmo("primary", self.pers["primaryspareammo"]);
	}
	
	// Secondary Weapon
	if (isDefined (self.pers["primaryb"]) )
	{
		self setWeaponSlotWeapon("primaryb", self.pers["primaryb"]);
		self setWeaponSlotClipAmmo("primaryb", self.pers["primarybmagazineammo"]);
		self setWeaponSlotAmmo("primaryb", self.pers["primarybspareammo"]);
	}
	
	// Pistol
	if (isDefined (self.pers["pistol"]) )
	{
		self setWeaponSlotWeapon("pistol", self.pers["pistol"]);
		self setWeaponSlotClipAmmo("pistol", self.pers["pistolmagazineammo"]);
		self setWeaponSlotAmmo("pistol", self.pers["pistolspareammo"]);
	}
	
	// Grenades
	if (isDefined (self.pers["grenade"]) )
	{
		self setWeaponSlotWeapon("grenade", self.pers["grenade"]);
		self setWeaponSlotAmmo("grenade", self.pers["grenadeammo"]);
	}
}

cm_clearPlayerWeapons()
{
	// Primary Weapon
	self.pers["primary"] = undefined;
    self.pers["primarymagazineammo"] = undefined;
    self.pers["primaryspareammo"] = undefined;
    
	// Secondary Weapon
    self.pers["primaryb"] = undefined;
    self.pers["primarybmagazineammo"] = undefined;
    self.pers["primarybspareammo"] = undefined;
    
	// Pistol
    self.pers["pistol"] = undefined;
    self.pers["pistolmagazineammo"] = undefined;
    self.pers["pistolspareammo"] = undefined;
    
	// Grenades
    self.pers["grenade"] = undefined;
    self.pers["grenadeammo"] = undefined;
}


cm_givePistolNoAmmo()
{
	// Get the player's current pistol
	pistolType = self getWeaponSlotWeapon("pistol");
	
	// If the player has a pistol
	// We don't want to replace it if they've purchased ammo for it
	if (pistolType != "none")
	{
		// If they do have a pistol
		// only give them a new one if they have less ammo than the starting amount
		currentPistolAmmo = self.pers["pistolmagazineammo"] + self.pers["pistolspareammo"];
		
		// Get that pistol's magazine size to calculate minimum ammo
		magazineSize = self cm_getWeaponMagazineSize(pistolType);
		
		minAmmo = 3 * magazineSize;
		
		if (currentPistolAmmo < minAmmo)
		{
			self setWeaponSlotClipAmmo("pistol", magazineSize);
			self setWeaponSlotAmmo("pistol", 2 * magazineSize);
		}
	}

	// If the player doesn't already have a pistol
	// Give them the default pistol and starting ammo
	else
	{
		// Default pistol is team dependent
		if(self.pers["team"] == "allies")
		{
			switch(game["allies"])		
			{
				case "american":
				case "british":
					pistolType = "colt_mp";
					magazineSize = cm_getWeaponMagazineSize(pistoltype);
					break;

				case "russian":
					pistolType = "luger_mp";
					magazineSize = cm_getWeaponMagazineSize(pistoltype);
					break;
			}
		}
		
		else if(self.pers["team"] == "axis")
		{
			switch(game["axis"])
			{
				case "german":
					pistolType = "luger_mp";
					magazineSize = cm_getWeaponMagazineSize(pistolType);
					break;
			}			
		}
		
		self giveWeapon(pistolType);
		self setWeaponSlotAmmo("pistol", 2 * magazineSize);
	}
	self.pers["pistol"] = pistolType;
}

cm_switchToMainWeapon()
{
	self switchToWeapon (self getWeaponSlotWeapon("grenade") );
	self switchToWeapon (self getWeaponSlotWeapon("pistol") );
	self switchToWeapon (self getWeaponSlotWeapon("primaryb") );
	self switchToWeapon (self getWeaponSlotWeapon("primary") );
}

cm_dropCurrentWeapon()
{
	self dropItem(self getcurrentweapon());
	self cm_switchToMainWeapon();
}





// Game Stuff ===============================================================================//

cm_setupGameVariables()
{
	game["cm_machineguns2"] = "cm_machineguns2";
	game["cm_rifles"] = "cm_rifles";
	
	precacheMenu(game["cm_machineguns2"]);
	precacheMenu(game["cm_rifles"]);

	game["allies_loseroundcount"] = 0;
	game["axis_loseroundcount"] = 0;
	game["hasdonehalftime"] = 0;
	
	game["bombdown"] = 0;
	
	game["maxplayermoney"] = 15000;
	game["startplayermoney"] = 10000;
}

cm_setupPlayer()
{
	self cm_clearPlayerMoney();
	self cm_clearPlayerWeapons();
	
	self.pers["isplaying"] = 0;
}


cm_withinGracePeriod()
{
	// getTime() gets the current game time since the start of the map
	// getStartTime() gets the time that the current round started at
	// both are in milliseconds, hence divide by 1000
	currentRoundTime = (getTime() - getStartTime())  / 1000;
	if (currentRoundTime <= level.graceperiod)
		return true;
	else
		return false;
}


cm_getWeaponMagazineSize(weapon)
{
	switch(weapon)
	{
		case "colt_mp": 
			return 7;
		case "m1carbine_mp":
			return 15;
		case "m1garand_mp":
			return 8;
		case "thompson_mp":
			return 20;
		case "bar_mp": 
			return 20;
		case "springfield_mp":
			return 5;
		case "enfield_mp":
			return 10;
		case "sten_mp":
			return 32;
		case "bren_mp": 
			return 30;
		case "mosin_nagant_mp":
			return 5;
		case "ppsh_mp":
			return 71;
		case "mosin_nagant_sniper_mp":
			return 5;
		case "luger_mp":
			return 8;
		case "kar98k_mp":
			return 5;
		case "mp40_mp":
			return 32;
		case "mp44_mp":
			return 30;
		case "kar98k_sniper_mp":
			return 5;
			
		default:
			return 15;
	}
}

cm_getWeaponMaxAmmo(weapon)
{
// This function returns the set max amount of spare ammo that a player
// can carry. It does not take into account ammo loaded into a weapon.
// It is used for limiting the number of magazines a player can carry.
	switch(weapon)
	{
		case "colt_mp": 
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "m1carbine_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "m1garand_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "thompson_mp":
			return (cm_getWeaponMagazineSize(weapon) * 2);
		case "bar_mp": 
			return (cm_getWeaponMagazineSize(weapon) * 2);
		case "springfield_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "enfield_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "sten_mp":
			return (cm_getWeaponMagazineSize(weapon) * 2);
		case "bren_mp": 
			return (cm_getWeaponMagazineSize(weapon) * 2);
		case "mosin_nagant_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "ppsh_mp":
			return (cm_getWeaponMagazineSize(weapon) * 2);
		case "mosin_nagant_sniper_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "luger_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "kar98k_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		case "mp40_mp":
			return (cm_getWeaponMagazineSize(weapon) * 2);
		case "mp44_mp":
			return (cm_getWeaponMagazineSize(weapon) * 2);
		case "kar98k_sniper_mp":
			return (cm_getWeaponMagazineSize(weapon) * 4);
		// Grenades have a default limit of 3
		// This is just for setting it lower than that
		case "grenade" :
			return 3;
			
		default:
			return 15;
	}
// Grenades have a limit of 3, set by the game
}


cm_getPrice(type, item)
{
	if (type == "weapon")
	{
		switch (item)
		{
			// machine guns
			case "sten_mp":
				return 1000;
			case "mp40_mp":
				return 1400;
			case "thompson_mp":
				return 1800;
			case "ppsh_mp":
				return 2500;
			case "bren_mp":
				return 2800;
			case "bar_mp":
				return 3100;
			case "mp44_mp":
				return 3400;
				
			// rifles
			case "m1carbine_mp":
				return 800;
			case "m1garand_mp":
				return 1100;
			case "enfield_mp":
				return 1800;
			case "mosin_nagant_mp":
				return 2200;
			case "kar98k_mp":
				return 2600;
			case "springfield_mp":
				return 2800;
			
			default :
				return 0;
		}
	}
	
	else if (type == "magazine")
	{
		switch (item)
		{
			// machine guns
			case "sten_mp":
				return 250;
			case "mp40_mp":
				return 300;
			case "thompson_mp":
				return 350;
			case "ppsh_mp":
				return 650;
			case "bren_mp":
				return 350;
			case "bar_mp":
				return 450;
			case "mp44_mp":
				return 500;
				
			// rifles
			case "m1carbine_mp":
				return 200;
			case "m1garand_mp":
				return 150;
			case "enfield_mp":
				return 120;
			case "mosin_nagant_mp":
				return 200;
			case "kar98k_mp":
				return 250;
			case "springfield_mp":
				return 300;
				
			// pistols
			case "colt_mp":
				return 70;
			case "luger_mp":
				return 80;
				
			default :
				return 0;
		}
	}
	
	else if (type == "grenade")
		return 600;
}

cm_getMoneyReward(event)
{
	switch (event)
	{
		case "frag" :
			return 300;
		
		case "loseroundbase" :
			return 1400;
		case "loseroundbonus" :
			return 500;
		
		case "allies_winrounddefault" :
			return 2400;
		case "allies_winroundplant" :
			return 3200;
		
		case "axis_winround" :
			return 2800;
			
		case "teamkill" :
			return -3000;
		
		default :
			return 0;
	}
}



cm_updateLoseRoundCounts(winner)
{
	if (winner == "allies")
	{
		game["allies_loseroundcount"]--;
		game["axis_loseroundcount"]++;
	}
	else
	{
		game["allies_loseroundcount"]++;
		game["axis_loseroundcount"]--;
	}
	
	if (game["allies_loseroundcount"] < 0)
		game["allies_loseroundcount"] = 0;
	else if (game["allies_loseroundcount"] > 5)
		game["allies_loseroundcount"] = 5;
		
	if (game["axis_loseroundcount"] < 0)
		game["axis_loseroundcount"] = 0;
	else if (game["axis_loseroundcount"] > 5)
		game["axis_loseroundcount"] = 5;
}

cm_checkForHalfTime()
{
	if (game["hasdonehalftime"] == 1)
		return;
		
	halfWay = level.roundlimit / 2;
	roundsPlayed = game["roundsplayed"];
	
	if (roundsPlayed >= halfWay )
	{
		game["hasdonehalftime"] = 1;
		
		level cm_doHalfTime();
	}
}

cm_doHalfTime()
{
	// Swap player stuff
	players = getentarray("player", "classname");
	
	for (i = 0; i < players.size; i++)
	{
		player = players[i];
		
		
		if (player.pers["team"] == "allies")
		{
			// Change the player's team
			player.pers["team"] = "axis";
			
			// Change icon stuff
			player.headicon = game["headicon_axis"];
			player.headiconteam = "axis";
			
			// Save the axis model for allied players
			axismodel = player.pers["savedmodel"];
		}
		else
		{
			// Change the player's team
			player.pers["team"] = "allies";	
			
			// Change icon stuff
			player.headicon = game["headicon_allies"];
			player.headiconteam = "allies";
			
			// Save the allied model for axis players
			alliedmodel = player.pers["savedmodel"];
		}
		
		// Reset money and weapons
		player cm_setupPlayer();
		
		player unlink();
		player enableWeapon();
	}
	
	// Use the saved models to update each player's model
	// This has to be done outside the previous loop so that each model is saved before it needs to be used
	for (i = 0; i < players.size; i++)
	{		
		if (player.pers["team"] == "allies")
		{
			player.pers["savedmodel"] = axismodel;
		}
		else
		{
			player.pers["savedmodel"] = alliedmodel;
		}
	}
	
	// Swap team scores
	tempScore = game["alliedscore"];
	
	game["alliedscore"] = game["axisscore"];
	setTeamScore("allies", game["axisscore"]);
	
	game["axisscore"] = tempScore;
	setTeamScore("axis", tempScore);
	
	// Announcements
	announcement("Half Time");
	wait(2);
	announcement("Swapping Teams");
	wait(2);
}





